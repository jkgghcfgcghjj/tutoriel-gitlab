Tutoriels:

- [Comment s'inscrire sur 
GitLab](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSCRIPTION_GITLAB.md)
- [Comment s'inscrire sur 
Slack](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSCRIPTION_SLACK.md)
- [Tutoriel GitLab](https://gitlab.com/DREES/tutoriels/blob/master/tutos/UTILISATION_GITLAB.md)
- [Comment installer Python 
(Anaconda)](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_PYTHON.md)
- [Comment installer git sous 
Windows](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_GIT_EXE.md)
- [Comment installer 
TortoiseGit](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_TORTOISEGIT.md)
- [Comment ajouter une clef RSA dans 
GitLab](https://gitlab.com/DREES/tutoriels/blob/master/tutos/AJOUT_CLE_PUB_GITLAB.md)
- [Introduction au concept de git en ligne de commande ou via 
TortoiseGit](http://www-cs-students.stanford.edu/~blynn/gitmagic/intl/fr/ch01.html)


Ces tutoriels se trouvent dans un projet public pour permettre leur visibilité depuis l'extérieur

Auteurs des tutos: EIG LabSanté & Stéphanie COMBES
